# El Juego de la Oca

El juego de la oca es un juego en el que dos o más jugadores mueven piezas alrededor de una tablero tirando un dado. El objetivo del juego es alcanzar el cuadrado número sesenta y tres antes que cualquiera de los otros jugadores y evitar obstáculos.

Tu misión es escribir un código al que le pasemos las ordenes del juego, y el vaya jugando y dandonos nuestra posicion en el tablero.

## Reglas básicas

- Puedes añadir jugadores:
  - Si no hay jugador, si le pasamos `add player Sara`, obtenemos `players: Sara`.
  - Si le pasamos `add player Juan`, obtenemos `players: Sara, Juan`.
- Sabe si un jugador esta duplicado:
  - Si le pasamos `add player Juan`, obtenemos `Juan: already existing player`.
- Podemos mover un jugador, pasandole el valor obtenido en la tidada de los dos dados:
  - Asumiendo que hay dos jugadores `Sara` y `Juan` en la casilla de salida:
    - Si le pasamos `move Sara 4, 2`, obtenemos `Sara rolls 4, 2. Sara moves from Start to 6`.
      - Si le pasamos `move Sara 2, 3`, obtenemos `Sara rolls 2, 3. Sara moves from 6 to 11`.
    - Si le pasamos `move Juan 2, 2`, obtenemos `Juan rolls 2, 2. Juan moves from Start to 4`.
- Ganamos si llegamos a la casilla 63:
  - Asumiendo que hay un jugador `Juan` en la casilla de 60:
    - Si le pasamos `move Juan 1, 2`, obtenemos `Juan rolls 1, 2. Juan moves from 60 to 63. Juan Wins!!`.
  - Hay rebote, solo ganamos si llegamos a la casilla 63 con la tirada exacta:
    - Si le pasamos `move Juan 3, 2`, obtenemos `Juan rolls 3, 2. Juan moves from 60 to 63. Juan bounces! Juan returns to 61`.

## El puente

Si un jugador cae en la casilla de `El puente`, salta a la casilla `12`:

- Si la jugadora Sara esta en la casilla `4`, y le pasamos `move Sara 1, 1`, obtenemos `Sara rolls 1, 1. Sara moves from 4 to The Bridge. Sara jumps to 12`.

## Dados autómaticos

Vamos a automatizar la tirada de dados. En una tirada autómatica ninguno de los dos valores podra ser superior a 12 ni inferior a 1.

- Si tenemos un jugador `Juan` esta en la casilla `4`, y le pasamos `move Juan`,
  obtenemos una tirada aleatoria, de por ejemplo 1 + 2, y veremos`Juan rolls 1, 2. Juan moves from 4 to 7`.

## Creditos

Está kata se ha obtenido de [katayuno.org](http://katayuno-app.herokuapp.com/katas/29) de [Devscola](https://devscola.org).
